﻿// CPPStudy.cpp : アプリケーションのエントリ ポイントを定義します。

#include "stdafx.h"

#include <cstdio>
#include <iostream>

#include <sstream>

using namespace std;

class TestClass
{
private:
	// デフォルトコンストラクタをprivateに指定することで、
	// メンバ変数への代入を強制化できる
	TestClass();
public:
	// main関数からはこのコンストラクタしか呼び出せない。
	TestClass(int a, int b, int c);
	// デストラクタ
	~TestClass();

	// Power用のGetterSetter
	int GetPower() { return Power; }
	bool SetPower(int value) {
		if (value < 0 || value > 100) return false;
		Power = value;
		return true;
	}
	// C用Getter Setter
	int GetC() { return C; }
	void SetC(int value) { C = value; }

	int B;
private:
	// ex 0~100%
	int Power;
	int C;
};

TestClass::TestClass()
{
	SetPower(10);
	this->B = 100;
	SetC(0);
}
TestClass::TestClass(int a, int b, int c) {
	TestClass();
	SetPower(a);
	this->B = b;
	SetC(c);
}

// デストラクタ
TestClass::~TestClass()
{
}

int main()
{
	// この呼び方は出来ない
	// TestClass test;

	TestClass test(10, 10, 100); // インスタンス生成時にコンストラクタが呼ばれる

	// コンストラクタが呼ばれているので、A,B,Cの初期化が完了している。
	cout << "test.A = " << test.GetPower() << endl;
	cout << "test.B = " << test.B << endl;
	cout << "test.C = " << test.GetC() << endl;

	// Setterの良さ
	// クラスを使う側のコードで危険な処理を行ってしまっても、
	// クラス関数でフィルタすることが出来る
	if (!test.SetPower(1000)) {
		cout << "代入失敗" << endl;
	}

	// 文字列構築
	std::ostringstream oss;
	oss << "test : " << 1000; // std::coutと同じ感じで使える
	cout << oss.str() << endl; // str()で取り出しchar *が必要なときはc_str()も使う

	oss.str(""); // ""セットで文字列クリア
	oss << "test2 : " << "faaa" << 123.234;
	cout << oss.str().c_str() << endl;

	// すぐ終わらないように
	getchar();
	return 0;
}